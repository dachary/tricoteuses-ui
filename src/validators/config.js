import { Legislature } from "@tricoteuses/assemblee/lib/types/legislatures"

import {
  validateChain,
  validateChoice,
  validateFunction,
  validateNonEmptyTrimmedString,
  validateUrl,
} from "@biryani/core"

export function validateAmenda(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateUrl,
      validateFunction(url => url.trim("/") + "/"),
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

export function validateAssembleeApi(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateUrl,
      validateFunction(url => url.trim("/") + "/"),
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

export function validateConfig(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  {
    const key = "amenda"
    remainingKeys.delete(key)
    const [value, error] = validateAmenda(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "assembleeApi"
    remainingKeys.delete(key)
    const [value, error] = validateAssembleeApi(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "legislature"
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateNonEmptyTrimmedString,
      validateChoice([Legislature.Quatorze, Legislature.Quinze]),
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "senatApi"
    remainingKeys.delete(key)
    const [value, error] = validateSenatApi(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of ["title", "url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

export function validateSenatApi(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateUrl,
      validateFunction(url => url.trim("/") + "/"),
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
