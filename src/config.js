import { validateConfig } from "./validators/config"

const config = {
  amenda: {
    url: "http://localhost:1791",
    // url: "https://amenda.tricoteuses.fr",
  },
  assembleeApi: {
    url: "http://localhost:1789",
    // url: "https://assemblee.api.tricoteuses.fr",
  },
  legislature: "15",
  senatApi: {
    url: "http://localhost:1792",
    // url: "https://senat.api.tricoteuses.fr",
  },
  title: "Tricoteuses (dev)",
  url: ".",
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validConfig
