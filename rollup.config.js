import babel from "rollup-plugin-babel"
import commonjs from "rollup-plugin-commonjs"
import copy from "rollup-plugin-copy"
import { eslint } from "rollup-plugin-eslint"
import json from "rollup-plugin-json"
import resolve from "rollup-plugin-node-resolve"
import postcss from "rollup-plugin-postcss"
import replace from "rollup-plugin-replace"
import svelte from "rollup-plugin-svelte"
import { terser } from "rollup-plugin-terser"
import config from "sapper/config/rollup.js"
import getPreprocessor from "svelte-preprocess"

import pkg from "./package.json"

const mode = process.env.NODE_ENV
const dev = mode === "development"
const legacy = !!process.env.SAPPER_LEGACY_BUILD

const onwarn = (warning, onwarn) =>
  (warning.code === "CIRCULAR_DEPENDENCY" &&
    /[/\\]@sapper[/\\]/.test(warning.message)) ||
  onwarn(warning)
const dedupe = importee => importee === "svelte" || importee.startsWith("svelte/")

const postcssPlugins = (purge = false) => {
  return [
    require("postcss-import")(),
    require("postcss-url")(),
    require("tailwindcss"),
    require("postcss-preset-env")({
      stage: 1,
      features: {
        "nesting-rules": true,
      },
    }),
    // Do not purge the CSS in dev mode to be able to play with classes in the
    // browser dev-tools.
    purge &&
      require("cssnano")({
        preset: "default",
      }),
    purge &&
      require("@fullhuman/postcss-purgecss")({
        content: ["./**/*.svelte"],
        extractors: [
          {
            extractor: content => {
              const fromClasses = content.match(/class:[A-Za-z0-9-_]+/g) || []

              return [
                ...(content.match(/[A-Za-z0-9-_:/]+/g) || []),
                ...fromClasses.map(c => c.replace("class:", "")),
              ]
            },
            extensions: ["svelte"],
          },
        ],
        // Whitelist selectors to stop Purgecss from removing them from your CSS.
        whitelist: [
          "html",
          "body",
          "ripple-gray",
          "ripple-primary",
          "ripple-white",
          "cursor-pointer",
          "navigation:hover",
          "navigation.selected",
          "outline-none",
          "text-xs",
          "transition",
        ],
        whitelistPatterns: [
          /bg-gray/,
          /text-gray/,
          /yellow-a200/,
          /language/,
          /namespace/,
          /token/,
          // These are from button examples, infer required classes.
          /(bg|ripple|text|border)-(red|teal|yellow|lime|primary)-(400|500|200|50)$/,
          // Added for Tricoteuses:
          /(bg|ripple|text|border)-(assemblee|conseil-etat|conseil-constitutionnel|gouvernement|presidence|senat)$/,
          /(bg|ripple|text|border)-(black|white|gray|red|orange|yellow|green|teal|blue|indigo|purple|pink)-\d+$/,
        ],
      }),
  ].filter(Boolean)
}

const preprocess = getPreprocessor({
  transformers: {
    postcss: {
      plugins: postcssPlugins(),
    },
  },
})

export default {
  client: {
    input: config.client.input(),
    output: config.client.output(),
    plugins: [
      replace({
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
      svelte({
        dev,
        emitCss: true,
        hydratable: true,
        preprocess,
      }),
      resolve({
        browser: true,
        dedupe,
      }),
      commonjs(),
      json(),

      legacy &&
        babel({
          extensions: [".js", ".mjs", ".html", ".svelte"],
          runtimeHelpers: true,
          exclude: ["node_modules/@babel/**"],
          presets: [
            [
              "@babel/preset-env",
              {
                targets: "> 0.25%, not dead",
              },
            ],
          ],
          plugins: [
            "@babel/plugin-syntax-dynamic-import",
            [
              "@babel/plugin-transform-runtime",
              {
                useESModules: true,
              },
            ],
          ],
        }),

      !dev &&
        terser({
          module: true,
        }),
    ],

    onwarn,
  },

  server: {
    input: config.server.input(),
    output: config.server.output(),
    plugins: [
      replace({
        "process.browser": false,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
      eslint({
        emitWarning: dev,
        fix: true,
      }),
      svelte({
        dev,
        generate: "ssr",
        preprocess,
      }),
      postcss({
        extract: "./static/global.css",
        plugins: postcssPlugins(!dev),
      }),
      copy({
        copyOnce: true,
        targets: [
          {
            src: "node_modules/typeface-roboto/files",
            dest: "static",
          },
          {
            src: "node_modules/material-icons/iconfont/MaterialIcons*",
            dest: "static",
          },
        ],
      }),
      resolve({
        dedupe,
      }),
      commonjs(),
      json(),
    ],
    external: Object.keys(pkg.dependencies).concat(
      require("module").builtinModules ||
        Object.keys(process.binding("natives")),
    ),

    onwarn,
  },

  serviceworker: {
    input: config.serviceworker.input(),
    output: config.serviceworker.output(),
    plugins: [
      resolve(),
      replace({
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
      commonjs(),
      !dev && terser(),
    ],

    onwarn,
  },
}
