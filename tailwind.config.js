const config = require("smelte/tailwind.config.js")

module.exports = {
  ...config,
  theme: {
    ...config.theme,
    colors: {
      ...config.theme.colors,
      // Color taken from http://www.assemblee-nationale.fr/
      "assemblee": "#023e6a",
      // Color taken from https://www.conseil-constitutionnel.fr/
      "conseil-constitutionnel": "#b29762",
      // Color taken from http://www.conseil-etat.fr/
      "conseil-etat": "#b1b3b4",
      // Color taken from https://www.gouvernement.fr/
      "gouvernement": "#202328",
      // Color taken from https://www.elysee.fr/
      "presidence": "#000",
      // Color taken from http://www.senat.fr
      "senat": "#c81f48",
    },
  },
}
