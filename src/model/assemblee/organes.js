import { CodeTypeOrgane } from "@tricoteuses/assemblee/lib/types/acteurs_et_organes"

export const fonctionsOrganes = [
  {
    code: "AppartenancePolitique",
    libelle: "Appartenance politique",
    libellePluriel: "Appartenance politique",
    typesOrganes: [
      {
        code: CodeTypeOrgane.Gp, // Groupe politique
        libelle: "Groupe politique",
        libellePluriel: "Groupes politiques",
      },
      {
        code: CodeTypeOrgane.Parpol, // Parti politique
        libelle: "Rattachement au titre du financement de la vie politique",
        libellePluriel: "Rattachements au titre du financement de la vie politique",
      },
    ],
  },
  {
    code: "FonctionAssembleeNationale",
    libelle: "Fonction à l'Assemblée nationale",
    libellePluriel: "Fonctions à l'Assemblée nationale",
    typesOrganes: [
      {
        code: CodeTypeOrgane.Assemblee,
        libelle: "Mandat",
        libellePluriel: "Mandats",
      },
      {
        code: CodeTypeOrgane.Cmp,
        libelle: "Commission mixte paritaire",
        libellePluriel: "Commissions mixtes paritaires",
      },
      {
        code: CodeTypeOrgane.Cnpe,
        libelle: "Commission d'enquête",
        libellePluriel: "Commissions d'enquête",
      },
      {
        code: CodeTypeOrgane.Cnps,
        libelle: "Commission spéciale",
        libellePluriel: "Commissions spéciales",
      },
      {
        code: CodeTypeOrgane.Comnl,
        libelle: "Commission spéciale (autre)",
        libellePluriel: "Commissions spéciales (autres)",
      },
      {
        code: CodeTypeOrgane.Comper,
        libelle: "Commission permanente",
        libellePluriel: "Commissions permanentes",
      },
      {
        code: CodeTypeOrgane.Confpt,
        libelle: "Conférence des présidents",
        libellePluriel: "Conférence des présidents",
      },
      {
        code: CodeTypeOrgane.Deleg,
        libelle: "Délégation",
        libellePluriel: "Délégations",
      },
      {
        code: CodeTypeOrgane.Delegbureau,
        libelle: "Délégation du Bureau",
        libellePluriel: "Délégations du Bureau",
      },
      {
        code: CodeTypeOrgane.Ga,
        libelle: "Groupe d'amitié",
        libellePluriel: "Groupes d'amitié",
      },
      {
        code: CodeTypeOrgane.Ge,
        libelle: "Groupe d'études",
        libellePluriel: "Groupes d'études",
      },
      {
        code: CodeTypeOrgane.Gevi,
        libelle: "Groupe d'études à vocation internationale",
        libellePluriel: "Groupes d'études à vocation internationale",
      },
      {
        code: CodeTypeOrgane.Misinfo,
        libelle: "Mission d'information",
        libellePluriel: "Missions d'information",
      },
      {
        code: CodeTypeOrgane.Misinfocom,
        libelle: "Mission d'information commune",
        libellePluriel: "Missions d'information commune",
      },
      {
        code: CodeTypeOrgane.Misinfopre,
        libelle: "Mission d'information prélable",
        libellePluriel: "Missions d'information prélable",
      },
      {
        code: CodeTypeOrgane.Offpar,
        libelle: "Office parlementaire",
        libellePluriel: "Offices parlementaires",
      },
    ],
  },
  {
    code: "FonctionLieeMandatDepute",
    libelle: "Fonction liée au mandat de député",
    libellePluriel: "Fonctions liées au mandat de député",
    typesOrganes: [
      {
        code: CodeTypeOrgane.Api,
        libelle: "Instance parlementaire internationale",
        libellePluriel: "Instances parlementaires internationales",
      },
      {
        code: CodeTypeOrgane.Cjr, // Cour de justice de la République
        libelle: "Instance judiciaire",
        libellePluriel: "Instances judiciaires",
      },
      {
        code: CodeTypeOrgane.Orgaint, // Organisme international,
        libelle: "Instance internationale",
        libellePluriel: "Instances internationales",
      },
      {
        code: CodeTypeOrgane.Orgextparl,
        libelle: "Organisme extra-parlementaire",
        libellePluriel: "Organismes extra-parlementaires",
      },
    ],
  },
  {
    code: "AutreFonction",
    libelle: "Autre fonction",
    libellePluriel: "Autres fonctions",
    typesOrganes: [
      {
        code: CodeTypeOrgane.Comsenat,
        libelle: "Commission sénatoriale",
        libellePluriel: "Commissions sénatoriales",
      },
      {
        code: CodeTypeOrgane.Comspsenat,
        libelle: "Commission spéciale sénatoriale",
        libellePluriel: "Commissions spéciales sénatoriales",
      },
      {
        code: CodeTypeOrgane.Constitu,
        libelle: "Conseil constitutionnel",
        libellePluriel: "Conseil constitutionnel",
      },
      {
        code: CodeTypeOrgane.Delegsenat,
        libelle: "Délégation sénatoriale",
        libellePluriel: "Délégations sénatoriales",
      },
      {
        code: CodeTypeOrgane.Gouvernement,
        libelle: "Gouvernement",
        libellePluriel: "Gouvernement",
      },
      {
        code: CodeTypeOrgane.Groupesenat, // Groupe sénatorial
        libelle: "Groupe politique du Sénat",
        libellePluriel: "Groupes politiques du Sénat",
      },
      {
        code: CodeTypeOrgane.Hcj, // Haute Cour de Justice
        libelle: "Haute Cour",
        libellePluriel: "Haute Cour",
      },
      {
        code: CodeTypeOrgane.Ministere,
        libelle: "Ministère",
        libellePluriel: "Ministères",
      },
      {
        code: CodeTypeOrgane.Presrep,
        libelle: "Présidence de la République",
        libellePluriel: "Présidence de la République",
      },
      {
        code: CodeTypeOrgane.Senat,
        libelle: "Sénat",
        libellePluriel: "Sénat",
      },
    ],
  },
]
