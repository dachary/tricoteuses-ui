import {
  CodeTypeOrgane,
  TypeMandat,
} from "@tricoteuses/assemblee/lib/types/acteurs_et_organes"

import config from "../../config"

export function getCommissionPermanente(acteur, now) {
  const { mandats } = acteur
  if (mandats === undefined) {
    return null
  }
  for (const mandat of mandats) {
    if (
      mandat.xsiType === TypeMandat.MandatSimpleType &&
      mandat.legislature === config.legislature &&
      mandat.typeOrgane === CodeTypeOrgane.Comper &&
      mandat.dateDebut <= now &&
      (!mandat.dateFin || mandat.dateFin >= now)
    ) {
      return mandat.organes[0]
    }
  }
  return null
}

export function getGroupePolitique(acteur, now) {
  const { mandats } = acteur
  if (mandats === undefined) {
    return null
  }
  for (const mandat of mandats) {
    if (
      mandat.xsiType === TypeMandat.MandatSimpleType &&
      mandat.legislature === config.legislature &&
      mandat.typeOrgane === CodeTypeOrgane.Gp &&
      mandat.dateDebut <= now &&
      (!mandat.dateFin || mandat.dateFin >= now)
    ) {
      return mandat.organes[0]
    }
  }
  return null
}
