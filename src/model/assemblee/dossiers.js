import {
  stateFromActePath,
  stateFromActes,
  statusFromCodierLibelle,
  StatusOnly,
} from "@tricoteuses/assemblee/lib/dossiers_legislatifs"
import { shortNameFromOrgane } from "@tricoteuses/assemblee/lib/organes"

export function colorFromCodierLibelle(libelle) {
  const status = statusFromCodierLibelle(libelle)
  return colorFromStatus(status)
}

export function colorFromOrganeCodeType(code) {
  if (code === null || code === undefined) {
    return null
  }
  return (
    {
      ASSEMBLEE: "assemblee",
      CE: "conseil-etat", // Conseil d'État
      CMP: "yellow-800", // Commission mixte paritaire
      CONSTITU: "conseil-constitutionnel", // Conseil constitutionnel
      GOUVERNEMENT: "gouvernement",
      INTERNET: "teal-500",
      PRESREP: "presidence", // Présidence de la République
      SENAT: "senat",
    }[code] || "yellow-800"
  )
}

export function colorFromStatus(status) {
  switch (status) {
    case StatusOnly.Failure:
      return "red-500"
    case StatusOnly.InProgress:
      return "gray-600"
    case StatusOnly.Success:
      return "green-500"
    default:
      return colorFromOrganeCodeType(status)
  }
}

export function nextActePath(dossier, now) {
  let nextDate = null
  let nextPath = null
  if (dossier.actesLegislatifs) {
    for (const acte of dossier.actesLegislatifs) {
      const [date, path] = nextDateAndPathForActe(acte, now)
      if (date && date >= now && (nextDate === null || date < nextDate)) {
        nextDate = date
        nextPath = path
      }
    }
  }
  return nextPath
}

export function nextDate(dossier, now) {
  let nextDate = null
  if (dossier.actesLegislatifs) {
    for (const acte of dossier.actesLegislatifs) {
      const date = nextDateAndPathForActe(acte, now)[0]
      if (date && date >= now && (nextDate === null || date < nextDate)) {
        nextDate = date
      }
    }
  }
  return nextDate
}

function nextDateAndPathForActe(acte, now) {
  let nextDate = acte.dateActe !== null && acte.dateActe >= now ? acte.dateActe : null
  let nextPath = []
  if (acte.actesLegislatifs) {
    for (const child of acte.actesLegislatifs) {
      const [date, path] = nextDateAndPathForActe(child, now)
      if (date && date >= now && (nextDate === null || date < nextDate)) {
        nextDate = date
        nextPath = path
      }
    }
  }
  if (nextDate === null) {
    nextPath = null
  } else {
    nextPath.unshift(acte)
  }
  return [nextDate, nextPath]
}

export function previousActePath(dossier, now) {
  let previousDate = null
  let previousPath = null
  if (dossier.actesLegislatifs) {
    for (const acte of [...dossier.actesLegislatifs].reverse()) {
      const [date, path] = previousDateAndPathForActe(acte, now)
      if (date && date < now && (previousDate === null || date > previousDate)) {
        previousDate = date
        previousPath = path
      }
    }
  }
  return previousPath
}

export function previousDate(dossier, now) {
  let previousDate = null
  if (dossier.actesLegislatifs) {
    for (const acte of [...dossier.actesLegislatifs].reverse()) {
      const date = previousDateAndPathForActe(acte, now)[0]
      if (date && date < now && (previousDate === null || date > previousDate)) {
        previousDate = date
      }
    }
  }
  return previousDate
}

function previousDateAndPathForActe(acte, now) {
  let previousDate = acte.dateActe !== null && acte.dateActe < now ? acte.dateActe : null
  let previousPath = []
  if (acte.actesLegislatifs) {
    for (const child of [...acte.actesLegislatifs].reverse()) {
      const [date, path] = previousDateAndPathForActe(child, now)
      if (date && date < now && (previousDate === null || date > previousDate)) {
        previousDate = date
        previousPath = path
      }
    }
  }
  if (previousDate === null) {
    previousPath = null
  } else {
    previousPath.unshift(acte)
  }
  return [previousDate, previousPath]
}

export function summaryStepFromActePath(dossier, actePath) {
  const rootActe = actePath[0]
  let rootOrgane = rootActe.organe
  let organeCodeType = rootOrgane === undefined ? undefined : rootOrgane.codeType
  let organeName = shortNameFromOrgane(rootOrgane)
  let title = rootActe.libelleActe.libelleCourt || rootActe.libelleActe.nomCanonique
  if (rootActe.codeActe === "CMP") {
    const level1Acte = actePath[1]
    if (level1Acte !== undefined && level1Acte.codeActe.startsWith("CMP-DEBATS-")) {
      const level1Organe = level1Acte.organe
      organeCodeType = level1Organe === undefined ? undefined : level1Organe.codeType
      organeName = shortNameFromOrgane(level1Organe)
      title = "Lecture du texte de la Commission mixte paritaire"
    }
  }
  if (title !== null && title.toLowerCase() === organeName.toLowerCase()) {
    title = null
  }
  let state = stateFromActePath(dossier, actePath)
  if (state !== null) {
    state = { ...state, color: colorFromStatus(state.currentStatus)}
  }
  return {
    color: colorFromOrganeCodeType(organeCodeType),
    organeName,
    state,
    title,
  }
}

export function summaryStepsFromDossier(dossier) {
  const summarySteps = []
  for (const rootActe of dossier.actesLegislatifs) {
    summaryStepsFromRootActe(dossier, rootActe, summarySteps)
  }
  return summarySteps
}

export function summaryStepsFromRootActe(dossier, rootActe, summarySteps) {
  let rootOrgane = rootActe.organe
  let organeCodeType = rootOrgane === undefined ? undefined : rootOrgane.codeType
  let organeName = shortNameFromOrgane(rootOrgane)
  let title = rootActe.libelleActe.libelleCourt || rootActe.libelleActe.nomCanonique
  if (title !== null && title.toLowerCase() === organeName.toLowerCase()) {
    title = null
  }
  let state = stateFromActes(dossier, [rootActe], rootActe.actesLegislatifs)
  if (state !== null) {
    state = { ...state, color: colorFromStatus(state.currentStatus)}
  }
  summarySteps.push({
    color: colorFromOrganeCodeType(organeCodeType),
    organeName,
    state,
    title,
  })
  if (rootActe.codeActe === "CMP") {
    for (const level1Acte of rootActe.actesLegislatifs || []) {
      if (level1Acte.codeActe.startsWith("CMP-DEBATS-")) {
        const level1Organe = level1Acte.organe
        organeCodeType = level1Organe === undefined ? undefined : level1Organe.codeType
        organeName = shortNameFromOrgane(level1Organe)
        title = "Lecture du texte de la Commission mixte paritaire"
        let state = stateFromActes(
          dossier,
          [rootActe, level1Acte],
          level1Acte.actesLegislatifs,
        )
        if (state !== null) {
          state = { ...state, color: colorFromStatus(state.currentStatus) }
        }
        summarySteps.push({
          color: colorFromOrganeCodeType(organeCodeType),
          organeName,
          state,
          title,
        })
      }
    }
  }
}

export function textFromTypeRapporteur(typeRapporteur) {
  return (
    {
      RAPPORTEUR: "rapporteur",
      RAPPORTEUR_GENERAL: "rapporteur général",
      RAPPORTEUR_POUR_AVIS: "rapporteur pour avis",
      RAPPORTEUR_SPECIAL: "rapporteur spécial",
    }[typeRapporteur] || typeRapporteur
  )
}
