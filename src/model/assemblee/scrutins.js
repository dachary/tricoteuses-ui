export function urlFromScrutin(scrutin) {
  // Sample UIDs: VTANR5L15V389
  const match = /^VTANR([0-9]+)L([0-9]+)V([0-9]+)$/.exec(scrutin.uid)
  if (match === null) {
    console.log(
      `Unable to generate URL from scrutin with unexpected UID: "${scrutin.uid}"`,
    )
    return null
  }
  const legislature = match[2]
  const numero = match[3]
  return `http://www2.assemblee-nationale.fr/scrutins/detail/(legislature)/${legislature}/(num)/${numero}`
}
