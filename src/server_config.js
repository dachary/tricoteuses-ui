require("dotenv").config()

import { validateServerConfig } from "./validators/server_config"

const serverConfig = {
  sessionSecret: process.env.TRICOTEUSES_UI_SESSION_SECRET,
}

const [validServerConfig, error] = validateServerConfig(serverConfig)
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      validServerConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validServerConfig
