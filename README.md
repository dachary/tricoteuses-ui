# Tricoteuses-UI

## _Live informations from french Parliament (Assemblée nationale & Sénat)_

![Screenshot of a Tricoteuses-UI](https://forum.parlement-ouvert.fr/uploads/default/original/1X/05bac0475f7e5946186a04713325a4bd35a5e125.png)

_Tricoteuses-UI_ is free and open source software.

* [software repository](https://framagit.org/tricoteuses/tricoteuses-ui)
* [GNU Affero General Public License version 3 or greater](https://framagit.org/tricoteuses/tricoteuses-ui/blob/master/LICENSE.md)

## Installation

### Install dependencies

```bash
npm install
```

## Server Configuration

Create a `.env` file to set configuration variables (you can use `example.env` as a template).

If you don't want to use your own [API server](https://framagit.org/tricoteuses/tricoteuses-api-assemblee), edit file `src/config.js` to change the API URL to the one of the [public API server](https://assemblee.api.tricoteuses.fr/):

```javascript
  ...
  api: {
    url: "https://assemblee.api.tricoteuses.fr",
  },
  ...
```

## Server Launch

In development mode:

```bash
npm run dev
```

In production mode:

```bash
npm run build
npm start
```
