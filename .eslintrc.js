module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: "eslint:recommended",
  globals: {
    Promise: true,
    Set: true,
  },
  parser: "babel-eslint",
  plugins: ["svelte3"],
  overrides: [
    {
      files: "*.svelte",
      processor: "svelte3/svelte3",
    },
  ],
  rules: {
    "comma-dangle": ["error", "always-multiline"],
    "max-len": [
      "error",
      { code: 90, ignoreRegExpLiterals: true, ignoreUrls: true, tabWidth: 2 },
    ],
    "no-undef": "error",
    "no-unused-expressions": "error",
    "no-unused-vars": "error",
    "no-use-before-define": ["error", "nofunc"],
    quotes: ["error", "double", "avoid-escape"],
    "require-atomic-updates": "off",
    semi: ["error", "never"],
  },
  // settings: {
  //   "svelte3/ignore-styles": () => true,
  // },
}
