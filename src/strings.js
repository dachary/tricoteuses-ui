export {
  capitalizeFirstLetter,
  uncapitalizeFirstLetter,
} from "@tricoteuses/assemblee/lib/strings"

export function buildSearchUrl(url, { limit = 10, offset = 0, term = "" }) {
  let query = []
  if (term) {
    query.push(
      `q=${encodeURIComponent(term)
        .replace("(", " ")
        .replace(")", " ")}`,
    )
  }
  if (offset !== 0) {
    query.push(`offset=${offset}`)
  }
  if (limit !== 10) {
    query.push(`limit=${limit}`)
  }
  query = query.join("&")
  if (query) {
    query = (url.includes("?") ? "&" : "?") + query
  }
  return url + query
}
