export function colorFromEtaloilib(etaloilib) {
  return etaloilib === "en cours de discussion"
    ? "gray-600"
    : etaloilib === "fusionné"
      ? "blue-500"
      : etaloilib === "rejeté"
        ? "red-500"
        : etaloilib === "promulgué ou adopté (ppr)"
          ? "green-500"
          : etaloilib === "caduc"
            ? "blue-500"
            : "yellow-500"
}
