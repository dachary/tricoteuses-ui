import { writable } from "svelte/store"

export const showNewIssueModal = writable(false)
