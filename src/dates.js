export {
  frenchDateFormat,
  frenchTimeFormat,
  localIso8601StringFromDate,
} from "@tricoteuses/assemblee/lib/dates"
export {
  displayDateFromLocalIsoString,
  frenchDateWithoutDayOfWeekFromDateOrString,
  frenchTimeFromDateOrString,
} from "@tricoteuses/dossiers-legislatifs/lib/dates"
import format from "date-fns/format"
import frLocale from "date-fns/locale/fr"

export function displayDateFromEpoch(epoch) {
  const date = new Date(epoch * 1000)
  let dateString = format(date, "EEEE d LLLL yyyy HH'h'mm", { locale: frLocale })
  // if (date.getUTCHours() === 0 && date.getUTCMinutes() === 0) {
  //   dateString = dateString.substring(0, dateString.length - 6)
  // }
  if (dateString.endsWith(" 00h00")) {
    dateString = dateString.substring(0, dateString.length - 6)
  }
  return dateString
}

export function frenchDateWithoutDayOfWeekFromEpoch(epoch) {
  const date = new Date(epoch * 1000)
  let dateString = format(date, "d LLLL yyyy HH'h'mm", { locale: frLocale })
  // if (date.getUTCHours() === 0 && date.getUTCMinutes() === 0) {
  //   dateString = dateString.substring(0, dateString.length - 6)
  // }
  if (dateString.endsWith(" 00h00")) {
    dateString = dateString.substring(0, dateString.length - 6)
  }
  return dateString
}

// export function frenchTimeFromEpoch(epoch) {
//   // const isoDate = new Date(epoch * 1000)
//   // const localDate = new Date((epoch + isoDate.getTimezoneOffset() * 60) * 1000)
//   const localDate = new Date(epoch * 1000)
//   return format(localDate, "H'h'mm", { locale: frLocale })
// }
