export function validateJsonResponse(validator) {
  return async function (response) {
    if (!response.ok && (response.status < 400 || response.status >= 404)) {
      return [await response.text(), `${response.status} ${response.statusText}`]
    }
    if (response.status === 204) {
      console.assert(validator === null)
      return [null, null]
    }
    let data = await response.json()
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }
    const error = data.error
    if (error) {
      if (error.details) {
        return [data, error.details]
      }
      return [data, error]
    }
    if (validator === null) {
      return [data, null]
    }
    return validator(data)
  }
}
