const documentUrlFragmentsByType = {
  ACIN: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ai",
  },
  AVCE: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ace",
  },
  AVIS: {
    directory: "rapports",
    prefix: "r",
    suffix: "",
  },
  ETDI: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ei",
  },
  LETT: {
    directory: "projets",
    prefix: "pl",
    suffix: "-l",
  },
  PION: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRE: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNREAPPART341: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRECOMENQ: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNREMODREGLTAN: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRETVXINSTITEUROP: {
    directory: "europe/resolutions",
    prefix: "ppe",
    suffix: "",
  },
  PRJL: {
    directory: "projets",
    prefix: "pl",
    suffix: "",
  },
  RAPP: {
    directory: "rapports",
    prefix: "r",
    suffix: "",
  },
  RINF: {
    directory: "rapports",
    prefix: "r",
    suffix: "",
  },
  RION: {
    directory: "",
    prefix: "",
    suffix: "",
  },
  TADO: {
    directory: "ta",
    prefix: "ta",
    suffix: "",
  },
  TCOM: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMCOMENQ: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMMODREGLTAN: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMTVXINSTITEUROP: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
}

export const classFromSubdivisionLevel = level => {
  switch (level) {
    case 1:
      // Tome
      return "text-5xl"
    case 2:
      // Partie
      return "text-4xl"
    case 3:
      // Livre
      return "text-4xl"
    case 4:
      // Titre
      return "text-4xl"
    case 5:
      // Chapitre
      return "text-3xl"
    case 6:
      // Section
      return "text-2xl"
    case 7:
      // Sous-section
      return "text-xl"
    case 8:
      // Paragraphe
      return "text-lg"
    case 9:
      // Article
      return "italic text-xl"
    default:
      return "text-lg"
  }
}

export function urlFromDocument(document, format) {
  // Code taken from function `an_text_url` in project anpy:
  // https://github.com/regardscitoyens/anpy/blob/master/anpy/dossier_from_opendata.py
  // See http://www.assemblee-nationale.fr/opendata/Implementation_Referentiel/Identifiants_Referentiels.html
  // for a description of the format of an UID.

  const extension = {
    html: ".asp",
    pdf: ".pdf",
    // The version of the document that can be parsed to extract divisions & articles
    "raw-html": ".asp",
  }[format]
  console.assert(
    extension !== undefined,
    `Unexpected format for urlFromDocument: ${format}`,
  )
  const match = /^(.{4})([ANS]*)(R[0-9])([LS]*)([0-9]*)([BTACP]*)(.*)$/.exec(document.uid)
  if (match === null) {
    console.log(
      `Unable to generate URL from document with unexpected UID: "${document.uid}"`,
    )
    return null
  }
  const legislature = match[5]
  let number = match[7]
  if (format === "raw-html") {
    return `http://www.assemblee-nationale.fr/${legislature}/textes/${number}${extension}`
  }
  const documentType =
    {
      BTC: "TCOM",
      BTA: "TADO",
    }[match[6]] || document.classification.type.code
  if (
    document.classification.sousType !== undefined &&
    document.classification.sousType.code === "COMPA"
  ) {
    // Annexe au rapport : Texte comparatif COMPA
    number = number.replace(/-COMPA/, "-aCOMPA")
  }
  const fragments = documentUrlFragmentsByType[documentType]
  if (fragments === undefined) {
    // For example: ALCNANR5L15B0002 (allocution du président)
    console.log(
      `Unable to generate URL from document of UID "${
        document.uid
      }" and type "${documentType}"`,
    )
    return null
  }
  const directory = format === "pdf" ? `pdf/${fragments.directory}` : fragments.directory
  return `http://www.assemblee-nationale.fr/${legislature}/${directory}/${
    fragments.prefix
  }${number}${fragments.suffix}${extension}`
}
