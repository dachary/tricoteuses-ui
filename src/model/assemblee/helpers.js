export function getAttribute(o, key, defaultValue) {
  if (o === undefined) {
    return defaultValue
  }
  const value = o[key]
  if (value === undefined) {
    return defaultValue
  }
  return value
}
